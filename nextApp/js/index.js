// app
var J_app = document.getElementById("j-app"),
    appCode = document.querySelector(".appcode");
J_app.onmouseover = function () {
    appCode.style.height = "148px";
    J_app.classList.add('active');
}
J_app.onmouseout = function () {
    appCode.style.height = "0";
    J_app.classList.remove('active');
}

/*购物车*/
var j_menu = document.querySelector(".j-menu"),
    site_Shop = document.querySelector(".site-shop"),
    cartColor = document.querySelector(".cart");
site_Shop.onmouseover = function () {
    j_menu.style.height = "99px";
    cartColor.classList.add("cart-color");
}
site_Shop.onmouseout = function () {
    j_menu.style.height = "0";
    cartColor.classList.remove("cart-color");
}

// 底部图片切换
var J_safeAuth = document.querySelector(".J_safeAuth");
setInterval(function () {
    J_safeAuth.classList.add("active");
}, 2000);
setInterval(function () {
    J_safeAuth.classList.remove("active");
}, 4000);