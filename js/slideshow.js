// 获取元素
var currentPage = 0,// 当前索引
    imgList = document.querySelectorAll('.slideshowList .item'),
    leftBtn = document.querySelector('.btnLeft'),
    rightBtn = document.querySelector('.btnRight'),
    aAll = document.querySelectorAll('.dot a'),
    dot = document.querySelector('.dot'),
    timer,
    slideshowList = document.querySelector('.slideshowList');

// 左箭头点击事件
leftBtn.onclick = function () {
    clearInterval(timer);
    currentPage--;
    if (currentPage < 0) {
        currentPage = imgList.length - 1;
    }
    imgList.forEach(function (item, index) {
        if (item.dataset.index == currentPage) {
            item.className = "item active";
            aAll[index].className = 'active';
        } else {
            item.className = "item";
            aAll[index].className = '';
        }
    })
}
// 右箭头点击事件
rightBtn.onclick = function () {
    clearInterval(timer);
    currentPage++;
    if (currentPage > imgList.length - 1) {
        currentPage = 0;
    }
    imgList.forEach(function (item, index) {
        if (item.dataset.index == currentPage) {
            item.className = "item active";
            aAll[index].className = 'active';
        } else {
            item.className = "item";
            aAll[index].className = '';
        }
    })
}
// 小圆点的点击事件
dot.onclick = function (event) {
    clearInterval(timer);
    // console.log(event);
    if (event.target.className != 'dot') {
        currentPage = event.target.dataset.index;
        imgList.forEach(function (item, index) {
            if (item.dataset.index == currentPage) {
                item.className = "item active";
                aAll[index].className = 'active';
            } else {
                item.className = "item";
                aAll[index].className = '';
            }
        })
    }
}

// 开启自动切换
autoChange()

// 封装自动切换的功能函数
function autoChange() {
    timer = setInterval(function () {
        currentPage++;
        if (currentPage > imgList.length - 1) {
            currentPage = 0;
        }
        imgList.forEach(function (item, index) {
            if (item.dataset.index == currentPage) {
                item.className = "item active";
                aAll[index].className = 'active';
            } else {
                item.className = "item";
                aAll[index].className = '';
            }
        })
    }, 3000)
}

slideshowList.onmouseover = function () {
    clearInterval(timer);
}

slideshowList.onmouseout = function () {
    clearInterval(timer);

    autoChange();
}

leftBtn.onmouseover = function () {
    clearInterval(timer);
}

leftBtn.onmouseout = function () {
    clearInterval(timer);

    autoChange();
}

rightBtn.onmouseover = function () {
    clearInterval(timer);
}

rightBtn.onmouseout = function () {
    clearInterval(timer);

    autoChange();
}

dot.onmouseover = function () {
    clearInterval(timer);
}

dot.onmouseout = function () {
    clearInterval(timer);

    autoChange();
}
