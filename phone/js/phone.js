//回顶部
var J_atop = document.getElementById("J_atop");
// 当网页向下滑动 854px 出现"返回顶部" 按钮
window.onscroll = function () { scrollFun() };

function scrollFun() {
    if (document.body.scrollTop > 854 || document.documentElement.scrollTop > 854) {
        J_atop.classList.add("active");
    } else {
        J_atop.classList.remove("active");
    }
}

function topFun() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

//点击返回顶部
J_atop.onclick = function () {
    topFun();
}